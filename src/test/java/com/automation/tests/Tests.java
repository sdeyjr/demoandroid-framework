package com.automation.tests;

import com.automation.pages.TDLandingPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.automation.core.TestBase.appiumDriver;

public class Tests {

    TDLandingPage tdLandingPage = null;

    @BeforeMethod
    public void setup() {
        tdLandingPage = PageFactory.initElements(appiumDriver, TDLandingPage.class);
    }

    @Test
    public void validate () {
        tdLandingPage.validateUserCanClickOnAccountButton();
    }

}
