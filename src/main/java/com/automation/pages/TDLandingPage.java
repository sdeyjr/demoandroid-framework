package com.automation.pages;

import com.automation.core.ExtentTestManager;
import com.automation.core.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TDLandingPage extends TestBase {

    @FindBy(xpath = "//android.widget.Button[@text='ACCOUNTS']")
    private WebElement accountButtonn;

    public void validateUserCanClickOnAccountButton() {
        accountButtonn.click();
        ExtentTestManager.log("Account button has been clicked");
    }
}
